# Cashier Registration

The service will expose an endpoint `\register` which will expect a json payload in the following format

```$xslt
{
"username": "johnDoe",
"password": "password",
"dob": "1990-05-09",
"paymentCardNumber: "1212456446464667446"
}
```

By default the application will use port 8080 and does not have any context path. The url to run the endpoint is 
`http://localhost:8080`. 
The port can be changes in `application.properties` file.

#### Pre requisites to start the application
The application used maven to build and please make sure maven in installed.
The application requires a postgres database with a database `postgres`. I am using both the username and password to be `postgres`.
If the application is run as a standalone jar, please make sure there is an instance of `postgres` running. The database name, username and password are
 configurable and can be change in `application.properties` file.

#### Running the application as docker container
The application is dockerised. The build the docker image please run
`mvn clean compile package docker:build`
It will create an image by the name `justin/app/cashier-registration`.
Please make sure docker is running in your machine before executing the above command.

 #### Running integration tests
 _Please note_: I have made provision to run a postgres container when starting the integration tests. If you have any other postgres instances running in
  your machine, please make sure to change the port of the postgres to something else in the `pom.xml`. By default, I am using the `host` network of docker
 .
 
 In order to run the integration test please run 
 `mvn clean verify` after building the image.
 This will first start a local postgres container called `database` and then start the service container called `cashier-registration-service`.
 If the test is failing, please try to increase the `wait` period for each container in the `io.fabric8` plugin execution in `pom.xml`.
 
 All the containers will be closed after the test are run.  
