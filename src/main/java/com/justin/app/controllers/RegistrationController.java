package com.justin.app.controllers;

import java.net.URI;

import javax.validation.Valid;

import com.justin.app.domain.UserFactory;
import com.justin.app.error_handling.ApplicationException;
import com.justin.app.rest.payload.RegisterUserPayload;
import com.justin.app.usecase.UseCase;
import com.justin.app.usecase.registration.RegisterUserRequest;
import com.justin.app.usecase.registration.RegisterUserResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
public class RegistrationController {

    private static final Logger LOG = LogManager.getLogger(RegistrationController.class);


    private final UseCase<RegisterUserRequest, RegisterUserResponse> useCase;
    private final UserFactory userFactory;

    public RegistrationController(UseCase<RegisterUserRequest, RegisterUserResponse> useCase, UserFactory userFactory) {
        this.useCase = useCase;
        this.userFactory = userFactory;
    }

    @PostMapping(value = "/register", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> check(@Valid @RequestBody RegisterUserPayload registerUserPayload, UriComponentsBuilder uriComponentsBuilder) {
        try {
            RegisterUserRequest registerUserRequest = userFactory.createRegisterUserRequest(registerUserPayload);
            RegisterUserResponse response = useCase.execute(registerUserRequest);
            final URI location = uriComponentsBuilder.path("/users/{userId}").build(response.getUserId());
            LOG.info("Successfully created user '{}' with userId '{}'", registerUserPayload.getUsername(), response.getUserId());
            return ResponseEntity.created(location).build();
        } catch (ApplicationException exception) {
            LOG.error("error processing registration request", exception.getErrorCode().getMessage());
            return ResponseEntity.status(exception.getErrorCode().getStatusCode()).build();
        }
    }
}
