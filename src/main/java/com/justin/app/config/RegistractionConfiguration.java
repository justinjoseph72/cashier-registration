package com.justin.app.config;

import com.justin.app.database.UserRepository;
import com.justin.app.database.UserRepositoryImpl;
import com.justin.app.database.jpa.BlockedIinEntityRepository;
import com.justin.app.database.jpa.UserEntityRepository;
import com.justin.app.domain.UserFactory;
import com.justin.app.service.HashingService;
import com.justin.app.service.Sha256HashingService;
import com.justin.app.service.UuidFactory;
import com.justin.app.usecase.UseCase;
import com.justin.app.usecase.registration.RegisterNewUserUseCase;
import com.justin.app.usecase.registration.RegisterUserRequest;
import com.justin.app.usecase.registration.RegisterUserResponse;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RegistractionConfiguration {

    @Bean
    public UuidFactory getUuidFactory() {
        return new UuidFactory();
    }

    @Bean
    public HashingService getHashingService() {
        return new Sha256HashingService();
    }

    @Bean
    public UserFactory getUserFactory(UuidFactory uuidFactory, HashingService hashingService) {
        return new UserFactory(uuidFactory, hashingService);
    }

    @Bean
    public UserRepository getUserRepository(UserEntityRepository userEntityRepository, BlockedIinEntityRepository blockedIinEntityRepository) {
        return new UserRepositoryImpl(userEntityRepository, blockedIinEntityRepository);
    }

    @Bean
    public UseCase<RegisterUserRequest, RegisterUserResponse> createUserRegistrationUseCase(UserFactory userFactory,
            UserRepository userRepository) {
        return new RegisterNewUserUseCase(userFactory, userRepository);
    }
}
