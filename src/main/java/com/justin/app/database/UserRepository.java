package com.justin.app.database;

public interface UserRepository {

    void save(UserInfo userInfo);

    UserInfo fetchByUserName(String userName);

    boolean isIinPresentInBlockedList(String iinValue);
}
