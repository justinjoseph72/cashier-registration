package com.justin.app.database;

import java.time.LocalDate;

public interface UserInfo {

    String getUserId();
    String getUserName();
    String getUserPassword();
    LocalDate getUserDateOfBirth();
    String getPaymentCardDetails();


}
