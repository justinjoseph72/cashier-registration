package com.justin.app.database.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BlockedIinEntityRepository extends JpaRepository<BlockedIinEntity, String> {

}
