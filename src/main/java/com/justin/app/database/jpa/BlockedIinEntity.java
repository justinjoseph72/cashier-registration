package com.justin.app.database.jpa;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Table(name = "blocked_iins")
@Entity(name = "blocked_iins")
public class BlockedIinEntity {

    @Id
    private String iin;

    public String getIin() {
        return iin;
    }

    public void setIin(String iin) {
        this.iin = iin;
    }
}
