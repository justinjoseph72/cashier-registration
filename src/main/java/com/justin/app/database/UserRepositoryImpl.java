package com.justin.app.database;

import com.justin.app.database.jpa.BlockedIinEntityRepository;
import com.justin.app.database.jpa.UserEntity;
import com.justin.app.database.jpa.UserEntityRepository;
import com.justin.app.usecase.registration.UserInfoImpl;

public class UserRepositoryImpl implements UserRepository {

    private final UserEntityRepository userRepository;
    private final BlockedIinEntityRepository iinRepository;


    public UserRepositoryImpl(UserEntityRepository userRepository, BlockedIinEntityRepository iinRepository) {
        this.userRepository = userRepository;
        this.iinRepository = iinRepository;
    }

    @Override
    public void save(UserInfo userInfo) {

        UserEntity userEntity = new UserEntity();
        userEntity.setUserId(userInfo.getUserId());
        userEntity.setUsername(userInfo.getUserName());
        userEntity.setPassword(userInfo.getUserPassword());
        userEntity.setDateOfBirth(userInfo.getUserDateOfBirth());
        userEntity.setPaymentCardDetails(userInfo.getPaymentCardDetails());

        userRepository.save(userEntity);

    }

    @Override
    public UserInfo fetchByUserName(String userName) {
        UserEntity userEntity = userRepository.findByUsername(userName);
        if(userEntity == null){
            return null;
        }
        return   new UserInfoImpl(userEntity.getUserId(), userEntity.getUsername(),
                userEntity.getPassword(), userEntity.getDateOfBirth(), userEntity.getPaymentCardDetails());
    }

    @Override
    public boolean isIinPresentInBlockedList(String iinValue) {
        return iinRepository.existsById(iinValue);
    }
}
