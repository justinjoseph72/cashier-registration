package com.justin.app.usecase;

public interface UseCaseRequest {

    boolean isValid();

}
