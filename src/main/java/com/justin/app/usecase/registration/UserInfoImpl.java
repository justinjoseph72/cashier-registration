package com.justin.app.usecase.registration;

import java.time.LocalDate;

import com.justin.app.database.UserInfo;

public class UserInfoImpl implements UserInfo {

    private final String userId;
    private final String userName;
    private final String userPassword;
    private final LocalDate userDateOfBirth;
    private final String paymentCardDetails;

    public UserInfoImpl(String userId, String userName, String userPassword, LocalDate userDateOfBirth, String paymentCardDetails) {
        this.userId = userId;
        this.userName = userName;
        this.userPassword = userPassword;
        this.userDateOfBirth = userDateOfBirth;
        this.paymentCardDetails = paymentCardDetails;
    }

    @Override
    public String getUserId() {
        return userId;
    }

    @Override
    public String getUserName() {
        return userName;
    }

    @Override
    public String getUserPassword() {
        return userPassword;
    }

    @Override
    public LocalDate getUserDateOfBirth() {
        return userDateOfBirth;
    }

    @Override
    public String getPaymentCardDetails() {
        return paymentCardDetails;
    }
}
