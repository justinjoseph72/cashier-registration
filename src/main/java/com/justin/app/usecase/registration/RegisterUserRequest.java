package com.justin.app.usecase.registration;

import java.time.LocalDate;

import com.google.common.base.Strings;
import com.justin.app.usecase.UseCaseRequest;

public class RegisterUserRequest implements UseCaseRequest {

    private final String userName;
    private final String password;
    private final LocalDate dateOfBirth;
    private final String cardNumber;

    public RegisterUserRequest(String userName, String password, LocalDate dateOfBirth, String cardNumber) {
        this.userName = userName;
        this.password = password;
        this.dateOfBirth = dateOfBirth;
        this.cardNumber = cardNumber;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    @Override
    public boolean isValid() {
        return !(Strings.isNullOrEmpty(userName) ||
                Strings.isNullOrEmpty(password) ||
                dateOfBirth == null ||
                Strings.isNullOrEmpty(cardNumber));
    }
}
