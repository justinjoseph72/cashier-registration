package com.justin.app.usecase.registration;

import com.justin.app.usecase.UseCaseResponse;

public class RegisterUserResponse implements UseCaseResponse {

    private final String userId;

    public RegisterUserResponse(String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

}
