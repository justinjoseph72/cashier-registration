package com.justin.app.usecase.registration;

import static com.justin.app.error_handling.ErrorCode.AGE_RESTRICTION;
import static com.justin.app.error_handling.ErrorCode.BLOCKED_CARD;
import static com.justin.app.error_handling.ErrorCode.MALFORMED_REQUEST;
import static com.justin.app.error_handling.ErrorCode.USER_ALREADY_REGISTERED;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import com.justin.app.database.UserInfo;
import com.justin.app.database.UserRepository;
import com.justin.app.domain.User;
import com.justin.app.domain.UserFactory;
import com.justin.app.error_handling.RegistrationException;
import com.justin.app.usecase.UseCase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class RegisterNewUserUseCase implements UseCase<RegisterUserRequest, RegisterUserResponse> {

    public static final int MIN_AGE = 18;
    private static final Logger LOG = LogManager.getLogger(RegisterNewUserUseCase.class);

    private final UserFactory userFactory;
    private final UserRepository userRepository;

    public RegisterNewUserUseCase(UserFactory userFactory, UserRepository userRepository) {
        this.userFactory = userFactory;
        this.userRepository = userRepository;
    }

    @Override
    public RegisterUserResponse execute(RegisterUserRequest useCaseRequest) {
        if (!useCaseRequest.isValid()) {
            LOG.debug("invalid request to process");
            throw new RegistrationException(MALFORMED_REQUEST, "Invalid request to register user");
        }

        if (isUserAgeLessThanMinAge(useCaseRequest.getDateOfBirth())) {
            LOG.info("User age is less than '{}' years", MIN_AGE);
            throw new RegistrationException(AGE_RESTRICTION, "User below minimum age limit");
        }

        UserInfo existingUser = userRepository.fetchByUserName(useCaseRequest.getUserName());
        if (existingUser != null) {
            LOG.info("Username '{}' already registered", useCaseRequest.getUserName());
            throw new RegistrationException(USER_ALREADY_REGISTERED, "User already registered");
        }

        final String iinNumber = getIinNumber(useCaseRequest.getCardNumber());
        if (userRepository.isIinPresentInBlockedList(iinNumber)) {
            LOG.info("The card for user '{}' is blocked", useCaseRequest.getUserName());
            throw new RegistrationException(BLOCKED_CARD, "User's card is blocked");
        }

        User user = userFactory.createUser(useCaseRequest);
        UserInfo userInfo = userFactory.createUserDbDetails(user);
        userRepository.save(userInfo);
        LOG.info("User successfully registered");
        return new RegisterUserResponse(user.getId());
    }

    private String getIinNumber(String cardNumber) {
        return cardNumber.substring(0, 6);
    }

    private boolean isUserAgeLessThanMinAge(LocalDate dateOfBirth) {
        LocalDate currentTime = LocalDate.now();
        return dateOfBirth.isAfter(currentTime.minus(MIN_AGE, ChronoUnit.YEARS));

    }


}
