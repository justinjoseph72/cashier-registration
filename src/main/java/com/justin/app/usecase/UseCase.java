package com.justin.app.usecase;

public interface UseCase<T extends UseCaseRequest, U extends UseCaseResponse> {

    U execute(T useCaseRequest);
}
