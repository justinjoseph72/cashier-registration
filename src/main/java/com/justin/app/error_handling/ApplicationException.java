package com.justin.app.error_handling;

public abstract class ApplicationException extends RuntimeException {

    public ApplicationException(String message){
        super(message);
    }

    public abstract ErrorCode getErrorCode();
}
