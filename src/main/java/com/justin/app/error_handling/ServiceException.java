package com.justin.app.error_handling;

public class ServiceException extends ApplicationException {

    public ServiceException(String message) {
        super(message);
    }

    public ErrorCode getErrorCode(){
        return ErrorCode.SERVER_ERROR;
    }
}
