package com.justin.app.error_handling;

public class RegistrationException extends ApplicationException {

    private ErrorCode errorCode;

    public RegistrationException(ErrorCode errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }
}
