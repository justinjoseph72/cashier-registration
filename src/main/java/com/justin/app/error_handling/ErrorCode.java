package com.justin.app.error_handling;

public enum ErrorCode {

    MALFORMED_REQUEST(400,"Invalid request"),
    AGE_RESTRICTION(403, "User under legal age limit"),
    USER_ALREADY_REGISTERED(409, "User name already used"),
    BLOCKED_CARD(406, "User's card is blocked"),
    SERVER_ERROR(500, "Server error!!! Please try again later");

    private final int statusCode;
    private final String message;

    ErrorCode(int statusCode, String message) {
        this.statusCode = statusCode;
        this.message = message;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getMessage() {
        return message;
    }
}
