package com.justin.app.rest.payload;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.justin.app.rest.validators.annotations.ValidDate;
import com.justin.app.rest.validators.annotations.ValidPassword;

public class RegisterUserPayload {

    @NotNull(message = "cannot be null")
    @NotEmpty(message = "cannot be empty")
    @Pattern(regexp = "^[a-zA-Z]+$",
            message = "does not match valid pattern")
    private String username;


    @ValidPassword
    private String password;

    @ValidDate
    private String dob;

    @Size(min = 15, max = 19)
    @Pattern(regexp = "^[0-9]*$",
            message = "should only contain digits")
    private String paymentCardNumber;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPaymentCardNumber() {
        return paymentCardNumber;
    }

    public void setPaymentCardNumber(String paymentCardNumber) {
        this.paymentCardNumber = paymentCardNumber;
    }
}
