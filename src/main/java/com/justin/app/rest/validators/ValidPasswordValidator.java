package com.justin.app.rest.validators;

import java.util.Arrays;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.google.common.base.Strings;
import com.justin.app.rest.validators.annotations.ValidPassword;
import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.LengthRule;
import org.passay.PasswordData;
import org.passay.PasswordValidator;

public class ValidPasswordValidator implements ConstraintValidator<ValidPassword, String> {

    @Override
    public boolean isValid(String password, ConstraintValidatorContext context) {
        if (Strings.isNullOrEmpty(password)) {
            return false;
        }
        PasswordValidator passwordValidator = new PasswordValidator(Arrays.asList(
                new LengthRule(4, 100),
                new CharacterRule(EnglishCharacterData.UpperCase, 1),
                new CharacterRule(EnglishCharacterData.Digit, 1)
        ));
        return passwordValidator.validate(new PasswordData(password)).isValid();
    }
}
