package com.justin.app.rest.validators;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.google.common.base.Strings;
import com.justin.app.rest.validators.annotations.ValidDate;

public class DateFormatValidator implements ConstraintValidator<ValidDate, String> {


    @Override
    public boolean isValid(String dateString, ConstraintValidatorContext context) {
        if (Strings.isNullOrEmpty(dateString)) {
            return false;
        }
        try {
            DateTimeFormatter.ISO_LOCAL_DATE.parse(dateString);
            return true;
        } catch (Exception exception) {
            return false;
        }
    }


}
