package com.justin.app.domain;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.justin.app.database.UserInfo;
import com.justin.app.rest.payload.RegisterUserPayload;
import com.justin.app.service.HashingService;
import com.justin.app.service.UuidFactory;
import com.justin.app.usecase.registration.RegisterUserRequest;
import com.justin.app.usecase.registration.UserInfoImpl;

public class UserFactory {

    public static final String DATE_FORMAT = "yyyy-MM-dd";
    private final UuidFactory uuidFactory;
    private final HashingService hashingService;

    public UserFactory(UuidFactory uuidFactory, HashingService hashingService) {
        this.uuidFactory = uuidFactory;
        this.hashingService = hashingService;
    }

    public User createUser(RegisterUserRequest request) {
        String userId = uuidFactory.createUuid();
        String hashedPassword = hashingService.createHash(request.getPassword());
        return new User(userId, request.getUserName(), hashedPassword, request.getDateOfBirth(), request.getCardNumber());
    }

    public UserInfo createUserDbDetails(User user) {
        return new UserInfoImpl(user.getId(), user.getUserName(), user.getPassword(), user.getDateOfBirth(), user.getPaymentCardNumber());
    }

    public RegisterUserRequest createRegisterUserRequest(RegisterUserPayload payload) {

        LocalDate dob = LocalDate.parse(payload.getDob(), DateTimeFormatter.ofPattern(DATE_FORMAT));
        return new RegisterUserRequest(payload.getUsername(),
                payload.getPassword(),
                dob,
                payload.getPaymentCardNumber());


    }
}
