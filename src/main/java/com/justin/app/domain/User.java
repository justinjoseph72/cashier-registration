package com.justin.app.domain;

import java.time.LocalDate;
import java.util.Date;

public final class User {

    private final String id;
    private final String userName;
    private final String password;
    private final LocalDate dateOfBirth;
    private final String paymentCardNumber;

    public User(String id, String userName, String password, LocalDate dateOfBirth, String paymentCardNumber) {
        this.id = id;
        this.userName = userName;
        this.password = password;
        this.dateOfBirth = dateOfBirth;
        this.paymentCardNumber = paymentCardNumber;
    }

    public String getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public String getPaymentCardNumber() {
        return paymentCardNumber;
    }

}
