package com.justin.app.service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.google.common.base.Strings;
import com.justin.app.error_handling.ServiceException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Hex;

public class Sha256HashingService implements HashingService {

    public static String MESSAGE_DIGEST_ALORITHM_SHA_256 = "SHA-256";

    private final MessageDigest digest;

    public Sha256HashingService() {
        try {
            digest = MessageDigest.getInstance(MESSAGE_DIGEST_ALORITHM_SHA_256, new BouncyCastleProvider());
        } catch (NoSuchAlgorithmException e) {
            throw new ServiceException("Unable to create the hashing digest");
        }
    }

    @Override
    public String createHash(String input) {
        if (Strings.isNullOrEmpty(input)) {
            return input;
        }

        byte[] hashedBytes = digest.digest(input.getBytes());
        return new String(Hex.encode(hashedBytes));
    }
}
