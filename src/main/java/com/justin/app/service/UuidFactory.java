package com.justin.app.service;

import java.util.UUID;

public class UuidFactory {

    public String createUuid() {
        return UUID.randomUUID().toString();
    }
}
