package com.justin.app.service;

public interface HashingService {

    String createHash(String input);
}
