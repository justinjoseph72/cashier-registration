DROP TABLE IF EXISTS user_details;
DROP TABLE IF EXISTS blocked_iins;

CREATE TABLE user_details (
user_id TEXT PRIMARY KEY,
username TEXT UNIQUE,
password TEXT NOT NULL,
date_of_birth TIMESTAMP NOT NULL,
payment_card_details TEXT NOT NULL

);

CREATE TABLE blocked_iins (
iin TEXT PRIMARY KEY
);

INSERT INTO blocked_iins (iin) VALUES ('234433');
INSERT INTO blocked_iins (iin) VALUES ('254633');
INSERT INTO blocked_iins (iin) VALUES ('235463');
INSERT INTO blocked_iins (iin) VALUES ('335563');
INSERT INTO blocked_iins (iin) VALUES ('735453');

