package com.justin.app.matchers;

import java.util.Set;

import javax.validation.ConstraintViolation;

import com.justin.app.rest.payload.RegisterUserPayload;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;

public class NoConstraintViolationMatcher extends TypeSafeDiagnosingMatcher<ConstraintViolation<RegisterUserPayload>> {

    private String property;

    private NoConstraintViolationMatcher(String property) {
        this.property = property;
    }

    public static NoConstraintViolationMatcher noViolationsFor(String property) {
        return new NoConstraintViolationMatcher(property);
    }


    @Override
    protected boolean matchesSafely(ConstraintViolation<RegisterUserPayload> item, Description mismatchDescription) {

        String propertyName = item.getPropertyPath().toString();
        String message = item.getMessage();
        mismatchDescription.appendText(String.format("there is a violation for %s with message %s", propertyName, message));
        return !property.equals(propertyName);

    }

    @Override
    public void describeTo(Description description) {
        description.appendText(String.format("No violation for property %s", property));
    }
}
