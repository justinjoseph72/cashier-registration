package com.justin.app.matchers;

import javax.validation.ConstraintViolation;

import com.justin.app.rest.payload.RegisterUserPayload;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;

public class ConstraintViolationMatcher extends TypeSafeDiagnosingMatcher<ConstraintViolation<RegisterUserPayload>> {

    private final String propertyName;
    private final String message;

    private ConstraintViolationMatcher(String propertyName, String message) {
        this.propertyName = propertyName;
        this.message = message;
    }

    public static ConstraintViolationMatcher forPropertyViolation(String propertyName, String message) {
        return new ConstraintViolationMatcher(propertyName, message);
    }


    @Override
    protected boolean matchesSafely(ConstraintViolation<RegisterUserPayload> item, Description mismatchDescription) {

        final String itemProperty = item.getPropertyPath().toString();
        final String itemMessage = item.getMessage();

        mismatchDescription.appendText(String.format("the property is %s and message is %s", itemProperty, itemMessage));
        return propertyName.equals(itemProperty) &&
                this.message.equals(itemMessage);
    }

    @Override
    public void describeTo(Description description) {
        description.appendText(String.format("the expected property is %s and message is %s", propertyName, message));
    }
}
