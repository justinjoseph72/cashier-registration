package com.justin.app.service;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import org.junit.Test;

public class HashingServiceTest {

    private HashingService service = new Sha256HashingService();



    @Test
    public void shouldReturnNullForNullInput() {
        String output = service.createHash(null);

        assertThat(output, nullValue());
    }

    @Test
    public void shouldReturnANonNullResultForNonNullInput() {
        String input = "a sample String";
        String output = service.createHash(input);

        assertThat(output, notNullValue());
    }

    @Test
    public void shouldReturnAnEmptyStringForEmptyInput() {
        String input = "";
        String output = service.createHash(input);

        assertThat(output, is(""));
    }

    @Test
    public void shouldReturnADifferentNotNullValueForNonEmptyInput() {
        String input = "a sample input";
        String output = service.createHash(input);

        assertThat(output, notNullValue());
        assertThat(output, not(input));
    }

}