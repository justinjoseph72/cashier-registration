package com.justin.app.rest.payload;

import static com.justin.app.matchers.ConstraintViolationMatcher.forPropertyViolation;
import static com.justin.app.matchers.NoConstraintViolationMatcher.noViolationsFor;
import static org.hamcrest.CoreMatchers.everyItem;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.hamcrest.Matchers;
import org.hamcrest.collection.IsEmptyCollection;
import org.hibernate.validator.HibernateValidator;
import org.junit.Test;

public class RegisterUserPayloadTest {

    private Validator validator = Validation.byProvider(HibernateValidator.class).configure()
            .buildValidatorFactory().getValidator();

    @Test
    public void shouldFailWithNullUserName() {
        RegisterUserPayload payload = new RegisterUserPayload();
        payload.setUsername(null);

        final Set<ConstraintViolation<RegisterUserPayload>> validate = validator.validate(payload);

        assertThat(validate, hasItem(forPropertyViolation("username", "cannot be null")));

    }

    @Test
    public void shouldFailWithEmptyUserName() {
        RegisterUserPayload payload = new RegisterUserPayload();
        payload.setUsername("");

        final Set<ConstraintViolation<RegisterUserPayload>> validate = validator.validate(payload);

        assertThat(validate, hasItem(forPropertyViolation("username", "cannot be empty")));
    }

    @Test
    public void shouldFailWithUserNameContainingDigits() {
        RegisterUserPayload payload = new RegisterUserPayload();
        payload.setUsername("sdf2");

        final Set<ConstraintViolation<RegisterUserPayload>> validate = validator.validate(payload);

        assertThat(validate, hasItem(forPropertyViolation("username", "does not match valid pattern")));
    }

    @Test
    public void shouldFailWithUserNameContainingSpaces() {
        RegisterUserPayload payload = new RegisterUserPayload();
        payload.setUsername("sdf s");

        final Set<ConstraintViolation<RegisterUserPayload>> validate = validator.validate(payload);

        assertThat(validate, hasItem(forPropertyViolation("username", "does not match valid pattern")));
    }

    @Test
    public void shouldPassWithUserNameContainingAllAlphanumericCharecters() {
        RegisterUserPayload payload = new RegisterUserPayload();
        payload.setUsername("johnCena");

        final Set<ConstraintViolation<RegisterUserPayload>> validate = validator.validate(payload);


        assertThat(validate, Matchers.everyItem(noViolationsFor("username")));

    }

    @Test
    public void shouldFailWhenPasswordIsNull() {
        RegisterUserPayload payload = new RegisterUserPayload();
        payload.setPassword(null);

        final Set<ConstraintViolation<RegisterUserPayload>> validate = validator.validate(payload);

        assertThat(validate, hasItem(forPropertyViolation("password", "invalid password")));

    }

    @Test
    public void shouldFailWhenPasswordIsEmpty() {
        RegisterUserPayload payload = new RegisterUserPayload();
        payload.setPassword("  ");

        final Set<ConstraintViolation<RegisterUserPayload>> validate = validator.validate(payload);

        assertThat(validate, hasItem(forPropertyViolation("password", "invalid password")));

    }

    @Test
    public void shouldFailWhenPasswordDoesNotHaveUpperCase() {
        RegisterUserPayload payload = new RegisterUserPayload();
        payload.setPassword("allsmallcasewithdigit1");

        final Set<ConstraintViolation<RegisterUserPayload>> validate = validator.validate(payload);

        assertThat(validate, hasItem(forPropertyViolation("password", "invalid password")));
    }

    @Test
    public void shouldFailWhenPasswordDoesNotHaveAnyDigit() {
        RegisterUserPayload payload = new RegisterUserPayload();
        payload.setPassword("ThisdoesnotHaveaDigit");

        final Set<ConstraintViolation<RegisterUserPayload>> validate = validator.validate(payload);

        assertThat(validate, hasItem(forPropertyViolation("password", "invalid password")));
    }

    @Test
    public void shouldFailWhenThereAreLessThanFourCharacters() {
        RegisterUserPayload payload = new RegisterUserPayload();
        payload.setPassword("Th3");

        final Set<ConstraintViolation<RegisterUserPayload>> validate = validator.validate(payload);

        assertThat(validate, hasItem(forPropertyViolation("password", "invalid password")));
    }

    @Test
    public void shouldPassWhenMinimumCriteriaIsMetForPassword() {
        RegisterUserPayload payload = new RegisterUserPayload();
        payload.setPassword("1Cu");

        final Set<ConstraintViolation<RegisterUserPayload>> validate = validator.validate(payload);

        assertThat(validate, hasItem(noViolationsFor("password")));
    }

    @Test
    public void shouldPassWhenAllCriteriaAreMetForPassword() {
        RegisterUserPayload payload = new RegisterUserPayload();
        payload.setPassword("YouShouldNowBeHappyFor1s");

        final Set<ConstraintViolation<RegisterUserPayload>> validate = validator.validate(payload);

        assertThat(validate, everyItem(noViolationsFor("password")));
    }

    @Test
    public void shouldFailWhenDateOfBirthIsNull() {
        RegisterUserPayload payload = new RegisterUserPayload();
        payload.setDob(null);

        final Set<ConstraintViolation<RegisterUserPayload>> validate = validator.validate(payload);

        assertThat(validate, hasItem(forPropertyViolation("dob", "invalid date of birth")));
    }

    @Test
    public void shouldFailWhenDateOfBirthIsEmpty() {
        RegisterUserPayload payload = new RegisterUserPayload();
        payload.setDob("");

        final Set<ConstraintViolation<RegisterUserPayload>> validate = validator.validate(payload);

        assertThat(validate, hasItem(forPropertyViolation("dob", "invalid date of birth")));
    }

    @Test
    public void shouldFailWhenDateOfBirthIsInvalidString() {
        RegisterUserPayload payload = new RegisterUserPayload();
        payload.setDob("sdfsdf");

        final Set<ConstraintViolation<RegisterUserPayload>> validate = validator.validate(payload);

        assertThat(validate, hasItem(forPropertyViolation("dob", "invalid date of birth")));
    }

    @Test
    public void shouldFailWhenDateOfBirthIsInvalidFormat() {
        RegisterUserPayload payload = new RegisterUserPayload();
        payload.setDob("2020/11/07");

        final Set<ConstraintViolation<RegisterUserPayload>> validate = validator.validate(payload);

        assertThat(validate, hasItem(forPropertyViolation("dob", "invalid date of birth")));
    }


    @Test
    public void shouldFailWhenDateOfBirthIsInvalidDateIsProvidedInCorrectFormat() {
        RegisterUserPayload payload = new RegisterUserPayload();
        payload.setDob("2020-16-07");

        final Set<ConstraintViolation<RegisterUserPayload>> validate = validator.validate(payload);

        assertThat(validate, hasItem(forPropertyViolation("dob", "invalid date of birth")));
    }

    @Test
    public void shouldPassWhenValidDateIsProvidedInCorrectFormat() {
        RegisterUserPayload payload = new RegisterUserPayload();
        payload.setDob("2020-06-07");

        final Set<ConstraintViolation<RegisterUserPayload>> validate = validator.validate(payload);

        assertThat(validate, everyItem(noViolationsFor("dob")));
    }

    @Test
    public void shouldFailWhenPaymentCardNumberHasAlphanumericCharacters() {
        RegisterUserPayload payload = new RegisterUserPayload();
        payload.setPaymentCardNumber("2020-06a07");

        final Set<ConstraintViolation<RegisterUserPayload>> validate = validator.validate(payload);
        assertThat(validate, hasItem(forPropertyViolation("paymentCardNumber", "should only contain digits")));

    }

    @Test
    public void shouldFailWhenPaymentCardNumberHasLessThan15Digits() {
        RegisterUserPayload payload = new RegisterUserPayload();
        payload.setPaymentCardNumber("20207");

        final Set<ConstraintViolation<RegisterUserPayload>> validate = validator.validate(payload);
        assertThat(validate, hasItem(forPropertyViolation("paymentCardNumber", "size must be between 15 and 19")));

    }

    @Test
    public void shouldFailWhenPaymentCardNumberHasMoreThan19Digits() {
        RegisterUserPayload payload = new RegisterUserPayload();
        payload.setPaymentCardNumber("12345678987654321123456");

        final Set<ConstraintViolation<RegisterUserPayload>> validate = validator.validate(payload);
        assertThat(validate, hasItem(forPropertyViolation("paymentCardNumber", "size must be between 15 and 19")));

    }


    @Test
    public void shouldPassWhenPaymentCardNumberHas17Digits() {
        RegisterUserPayload payload = new RegisterUserPayload();
        payload.setPaymentCardNumber("12345678985457424");

        final Set<ConstraintViolation<RegisterUserPayload>> validate = validator.validate(payload);
        assertThat(validate, everyItem(noViolationsFor("paymentCardNumber")));

    }


    @Test
    public void shouldHaveNoViolationsWhenAllThePropertiesSatisfyConstraints() {
        RegisterUserPayload payload = new RegisterUserPayload();
        payload.setUsername("MojoJojo");
        payload.setPassword("1VeryInSecureButValidPassword");
        payload.setDob("2020-06-07");
        payload.setPaymentCardNumber("12345678985457424");

        final Set<ConstraintViolation<RegisterUserPayload>> validate = validator.validate(payload);
        assertThat(validate, IsEmptyCollection.empty());

    }

}