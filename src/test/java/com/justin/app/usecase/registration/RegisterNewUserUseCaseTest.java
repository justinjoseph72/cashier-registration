package com.justin.app.usecase.registration;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import com.justin.app.database.UserInfo;
import com.justin.app.database.UserRepository;
import com.justin.app.domain.UserFactory;
import com.justin.app.service.HashingService;
import com.justin.app.service.UuidFactory;
import com.justin.app.error_handling.RegistrationException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class RegisterNewUserUseCaseTest {

    public static final String SOME_UUID = "someRandomUuid";
    public static final String SOME_OTHER_UUID = "someRandomUuid";
    public static final String HASHED_PASSWORD = "hashedPassword";

    @Mock UuidFactory uuidFactory;
    @Mock HashingService hashingService;
    @Mock UserFactory userFactory;
    @Mock UserRepository userRepository;
    @Mock UserInfo fetchedUserDetails;
    @Captor ArgumentCaptor<UserInfoImpl> userDetailsCaptor;

    private RegisterNewUserUseCase testService;
    private RegisterUserRequest request;
    private LocalDate currentDate = LocalDate.now();

    @Before
    public void init() {
        LocalDate dateOfBirth = currentDate.minus(30, ChronoUnit.YEARS);
        request = new RegisterUserRequest("name", "password", dateOfBirth, "112312525454464");
        Mockito.when(hashingService.createHash(Mockito.anyString())).thenReturn(HASHED_PASSWORD);
        Mockito.when(uuidFactory.createUuid()).thenReturn(SOME_UUID, SOME_OTHER_UUID);
        Mockito.when(userRepository.fetchByUserName(request.getUserName())).thenReturn(null, fetchedUserDetails);
        Mockito.when(userRepository.isIinPresentInBlockedList("112312")).thenReturn(false);


        userFactory = new UserFactory(uuidFactory, hashingService);
        testService = new RegisterNewUserUseCase(userFactory, userRepository);
    }

    @Test(expected = RegistrationException.class)
    public void shouldFailWhenRequestIsInvalid() {
        RegisterUserRequest request = new RegisterUserRequest(null, null, null, "");

        testService.execute(request);
    }

    @Test(expected = RegistrationException.class)
    public void shouldFailWhenRequestIsIncomplete() {
        RegisterUserRequest request = new RegisterUserRequest("name", "password", LocalDate.now(), "");

        testService.execute(request);
    }

    @Test(expected = RegistrationException.class)
    public void shouldFailWhenUserIsUnder18YearsOld() {
        LocalDate currentDate = LocalDate.now();
        LocalDate dateOfBirth = currentDate.minus(17, ChronoUnit.YEARS);
        RegisterUserRequest request = new RegisterUserRequest("anotherName", "anotherPassword", dateOfBirth, "222322");

        testService.execute(request);

    }

    @Test
    public void shouldSuccessfullyCreateAUserWithValidData() {
        RegisterUserResponse response = testService.execute(request);

        assertThat(response, notNullValue());
        assertThat(response.getUserId(), notNullValue());
    }

    @Test
    public void shouldReturnTheUserIdOfTheNewRegisteredUserWhenValidInputIsProvided() {
        RegisterUserResponse response = testService.execute(request);

        assertThat(response.getUserId(), is(SOME_UUID));
    }

    @Test(expected = RegistrationException.class)
    public void shouldFailWhenAUserNameAlreadyExists() {
        testService.execute(request);

        RegisterUserRequest newUserRequest = new RegisterUserRequest("name", "password", currentDate.minus(25, ChronoUnit.YEARS), "343434");
        testService.execute(newUserRequest);
    }

    @Test
    public void shouldReturnDifferentUserIdForDifferentUsers() {
        RegisterUserResponse response = testService.execute(request);

        assertThat(response.getUserId(), is(SOME_UUID));

        RegisterUserRequest request1 = new RegisterUserRequest("anotherName", "anotherPassword", currentDate.minus(25, ChronoUnit.YEARS), "222322");
        RegisterUserResponse response2 = testService.execute(request1);

        assertThat(response2.getUserId(), is(SOME_OTHER_UUID));

    }

    @Test
    public void shouldSaveTheUserDetailsIntoDatabase() {
        testService.execute(request);

        Mockito.verify(userRepository).save(userDetailsCaptor.capture());

        UserInfoImpl userDbDetailsImpl = userDetailsCaptor.getValue();
        assertThat(userDbDetailsImpl.getUserId(), is(SOME_UUID));
        assertThat(userDbDetailsImpl.getUserName(), is(request.getUserName()));
        assertThat(userDbDetailsImpl.getPaymentCardDetails(), is(request.getCardNumber()));
        assertThat(userDbDetailsImpl.getUserDateOfBirth(), is(request.getDateOfBirth()));
        assertThat(userDbDetailsImpl.getUserPassword(), is(HASHED_PASSWORD));

    }

    @Test(expected = RegistrationException.class)
    public void shouldFailWhenUserIinIsBlocked() {
        LocalDate dateOfBirth = currentDate.minus(30, ChronoUnit.YEARS);
        RegisterUserRequest request = new RegisterUserRequest("name", "password", dateOfBirth, "111111525454464");
        Mockito.when(userRepository.isIinPresentInBlockedList("111111")).thenReturn(true);
        testService.execute(request);

    }



}