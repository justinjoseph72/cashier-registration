package com.justin.app.integration;

import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_CONFLICT;
import static java.net.HttpURLConnection.HTTP_CREATED;
import static java.net.HttpURLConnection.HTTP_FORBIDDEN;
import static java.net.HttpURLConnection.HTTP_NOT_ACCEPTABLE;

import com.justin.app.integration.payload.RegisterUserPayload;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.BeforeClass;
import org.junit.Test;

public class RegistrationIT {

    @BeforeClass
    public static void init(){
        RestAssured.baseURI = "http://localhost:8080/";
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
    }


    @Test
    public void shouldFailWithBadRequestWhenNoBodyIsProvided(){
        RestAssured
                .given()
                    .header("Content-Type", ContentType.JSON)
                .when()
                    .post("/register")
                .then()
                    .statusCode(HTTP_BAD_REQUEST);
    }

    @Test
    public void shouldFailWithBadRequestWhenUserNameIsNotProvided() {
        RegisterUserPayload payload = RegisterUserPayload.validPayload().withName("").build();
        RestAssured
                .given()
                    .header("Content-Type", ContentType.JSON)
                    .body(payload.encode())
                .when()
                    .post("/register")
                .then()
                    .statusCode(HTTP_BAD_REQUEST);
    }

    @Test
    public void shouldFailWithBadRequestWhenPasswordIsNotProvided() {
        RegisterUserPayload payload = RegisterUserPayload.validPayload().withPassword("").build();
        RestAssured
                .given()
                    .header("Content-Type", ContentType.JSON)
                    .body(payload.encode())
                .when()
                    .post("/register")
                .then()
                    .statusCode(HTTP_BAD_REQUEST);
    }

     @Test
    public void shouldFailWithBadRequestWhenPasswordProvidedIsNotOfMinimumLength() {
        RegisterUserPayload payload = RegisterUserPayload.validPayload().withPassword("abc").build();
        RestAssured
                .given()
                    .header("Content-Type", ContentType.JSON)
                    .body(payload.encode())
                .when()
                    .post("/register")
                .then()
                    .statusCode(HTTP_BAD_REQUEST);
    }

     @Test
    public void shouldFailWithBadRequestWhenUserPasswordProvidedIsDoesNotHaveAnyUpperCaseCharacter() {
        RegisterUserPayload payload = RegisterUserPayload.validPayload().withPassword("abcdef").build();
        RestAssured
                .given()
                    .header("Content-Type", ContentType.JSON)
                    .body(payload.encode())
                .when()
                    .post("/register")
                .then()
                    .statusCode(HTTP_BAD_REQUEST);
    }

    @Test
    public void shouldFailWithBadRequestWhenUserPasswordProvidedIsDoesNotHaveAnyDigit() {
        RegisterUserPayload payload = RegisterUserPayload.validPayload().withPassword("abcdefK").build();
        RestAssured
                .given()
                    .header("Content-Type", ContentType.JSON)
                    .body(payload.encode())
                .when()
                    .post("/register")
                .then()
                    .statusCode(HTTP_BAD_REQUEST);
    }

    @Test
    public void shouldFailWithBadRequestWhenPasswordProvidedIsDoesNotHaveAnyDigit() {
        RegisterUserPayload payload = RegisterUserPayload.validPayload().withPassword("abcdefK").build();
        RestAssured
                .given()
                    .header("Content-Type", ContentType.JSON)
                    .body(payload.encode())
                .when()
                    .post("/register")
                .then()
                    .statusCode(HTTP_BAD_REQUEST);
    }

     @Test
    public void shouldFailWithBadRequestWhenDateOfBirthProvidedIsInvalidString() {
        RegisterUserPayload payload = RegisterUserPayload.validPayload().withPassword("abcdefK").build();
        RestAssured
                .given()
                    .header("Content-Type", ContentType.JSON)
                    .body(payload.encode())
                .when()
                    .post("/register")
                .then()
                    .statusCode(HTTP_BAD_REQUEST);
    }

     @Test
    public void shouldFailWithBadRequestWhenDateOfBirthProvidedIsInvalidDate() {
        RegisterUserPayload payload = RegisterUserPayload.validPayload().withPassword("1911-55-96").build();
        RestAssured
                .given()
                    .header("Content-Type", ContentType.JSON)
                    .body(payload.encode())
                .when()
                    .post("/register")
                .then()
                    .statusCode(HTTP_BAD_REQUEST);
    }


    @Test
    public void shouldFailWithBadRequestWhenPaymentCardNumberIsShorterThan15Digits() {
        RegisterUserPayload payload = RegisterUserPayload.validPayload().withPassword("19115596").build();
        RestAssured
                .given()
                    .header("Content-Type", ContentType.JSON)
                    .body(payload.encode())
                .when()
                    .post("/register")
                .then()
                    .statusCode(HTTP_BAD_REQUEST);
    }

    @Test
    public void shouldFailWithBadRequestWhenPaymentCardNumberIsLongerThan19Digits() {
        RegisterUserPayload payload = RegisterUserPayload.validPayload().withPassword("1144558877665442254458778455487545445").build();
        RestAssured
                .given()
                    .header("Content-Type", ContentType.JSON)
                    .body(payload.encode())
                .when()
                    .post("/register")
                .then()
                    .statusCode(HTTP_BAD_REQUEST);
    }

     @Test
    public void shouldFailWithBadRequestWhenPaymentCardNumberIsNotDigits() {
        RegisterUserPayload payload = RegisterUserPayload.validPayload().withPassword("asedvsgwsdfsdfwsdsdfsd").build();
        RestAssured
                .given()
                    .header("Content-Type", ContentType.JSON)
                    .body(payload.encode())
                .when()
                    .post("/register")
                .then()
                    .statusCode(HTTP_BAD_REQUEST);
    }

    @Test
    public void shouldFailWithForbiddenWhenUserAgeIsLessThan18Years(){
        RegisterUserPayload payload = RegisterUserPayload.validPayload().withDateOfBirth("2020-01-01").build();
        RestAssured
                .given()
                    .header("Content-Type", ContentType.JSON)
                    .body(payload.encode())
                .when()
                    .post("/register")
                .then()
                    .statusCode(HTTP_FORBIDDEN);
    }


    @Test
    public void shouldSucceedWithValidPayload() {
        RegisterUserPayload payload = RegisterUserPayload.validPayload().build();
        RestAssured
                .given()
                    .header("Content-Type", ContentType.JSON)
                    .body(payload.encode())
                .when()
                    .post("/register")
                .then()
                    .statusCode(HTTP_CREATED);
    }


    @Test
    public void shouldFailWithConflictWhenUserNameHasAlreadyBeenRegistered() {
        RegisterUserPayload payload = RegisterUserPayload.validPayload()
                .withName("johnMason").build();
        RestAssured
                .given()
                    .header("Content-Type", ContentType.JSON)
                    .body(payload.encode())
                .when()
                    .post("/register")
                .then()
                    .statusCode(HTTP_CREATED);

        RestAssured
                .given()
                    .header("Content-Type", ContentType.JSON)
                    .body(payload.encode())
                .when()
                    .post("/register")
                .then()
                    .statusCode(HTTP_CONFLICT);
    }

    @Test
    public void shouldReturnNotAcceptableWhenTheUserCardIsBlocked() {
        RegisterUserPayload payload = RegisterUserPayload.validPayload()
                .withName("tripleH")
                .withPaymentCard("234433857845561245")
                .build();

        RestAssured
                .given()
                    .header("Content-Type", ContentType.JSON)
                    .body(payload.encode())
                .when()
                    .post("/register")
                .then()
                    .statusCode(HTTP_NOT_ACCEPTABLE);
    }



}
