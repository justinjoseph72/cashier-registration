package com.justin.app.integration.payload;

import com.fasterxml.jackson.databind.JsonNode;

public interface JsonPayload {

    JsonNode asJson();

    String encode();

}
