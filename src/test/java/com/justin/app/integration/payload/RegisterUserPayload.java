package com.justin.app.integration.payload;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class RegisterUserPayload implements JsonPayload {

    private ObjectNode json;

    private RegisterUserPayload() {
        json = new ObjectMapper().createObjectNode();
    }

    public static RegisterPayloadBuilder validPayload(){
        return RegisterUserPayload.builder().withName("johnDoe")
                .withPassword("compLetePassword1")
                .withDateOfBirth("1990-05-08")
                .withPaymentCard("112233445533226644");
    }

    public static RegisterPayloadBuilder builder() {
        return new RegisterPayloadBuilder();
    }

    @Override
    public JsonNode asJson() {
        return json;
    }

    @Override
    public String encode() {
        return json.toPrettyString();
    }

    public static class RegisterPayloadBuilder {

        RegisterUserPayload hallPayload;

        public RegisterPayloadBuilder() {
            hallPayload = new RegisterUserPayload();
        }

        public RegisterPayloadBuilder withName(String name) {
            hallPayload.json.put("username", name);
            return this;
        }

        public RegisterPayloadBuilder withPassword(String password) {
            hallPayload.json.put("password", password);
            return this;
        }

        public RegisterPayloadBuilder withDateOfBirth(String dateOfBirth) {
            hallPayload.json.put("dob", dateOfBirth);
            return this;
        }

        public RegisterPayloadBuilder withPaymentCard(String paymentCardNumber) {
            hallPayload.json.put("paymentCardNumber", paymentCardNumber);
            return this;
        }


        public RegisterUserPayload build() {
            return hallPayload;
        }
    }


}
